package fr.jezequel.dupont;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;


public class AddItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText e = (EditText) findViewById(R.id.editText);
                String tache = e.getText().toString();
                RadioGroup r = (RadioGroup) findViewById(R.id.radioGroup2);
                TodoItem.Tags ttt = TodoItem.Tags.Faible;

                int b1 = findViewById(R.id.radioF).getId();
                int b2 = findViewById(R.id.radioN).getId();
                int b3 = findViewById(R.id.radioI).getId();

                if(b1 == r.getCheckedRadioButtonId())
                    ttt = TodoItem.Tags.Faible;
                if(b2 == r.getCheckedRadioButtonId())
                    ttt = TodoItem.Tags.Normal;
                if(b3 == r.getCheckedRadioButtonId())
                    ttt = TodoItem.Tags.Important;

                TodoItem ti = new TodoItem(ttt, tache);

                TodoDbHelper.addItem(ti, getBaseContext());
                Intent main = new Intent(AddItem.this, MainActivity.class);
                startActivity(main);
            }
        });
    }

}
